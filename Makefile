#------------------------------------------------------------------
# Project build information
#------------------------------------------------------------------
PROJNAME := atlantis
ATLANTIS_VERSION := v0.30.0

IMAGE := $(PROJNAME):$(ATLANTIS_VERSION)

#------------------------------------------------------------------
# Local targets
#------------------------------------------------------------------

build:
	docker build \
	--build-arg ATLANTIS_VERSION=$(ATLANTIS_VERSION) \
	--no-cache \
	-t $(IMAGE) .

scan:
	trivy --version
	# cache cleanup is needed when scanning images with the same tags, it does not remove the database
	time trivy image --clear-cache
	# update vulnerabilities db
	time trivy image --download-db-only
    # Prints full report
	trivy image --ignore-policy trivy-ignore.rego --light -s "UNKNOWN,MEDIUM,HIGH,CRITICAL" --exit-code 1 $(IMAGE)

#------------------------------------------------------------------
# CI targets
#------------------------------------------------------------------

build-and-save: build
	mkdir -p image
	docker save $(IMAGE) > image/$(IMAGE).tar

load:
	docker load -i  image/$(IMAGE).tar

load-and-scan: load scan-ci

push: build
	docker login -u $(CI_REGISTRY_USER) -p $(CI_REGISTRY_PASSWORD) $(CI_REGISTRY)
	docker tag $(IMAGE) $(CI_REGISTRY_IMAGE)/$(IMAGE)-$(CI_COMMIT_SHORT_SHA)
	docker push $(CI_REGISTRY_IMAGE)/$(IMAGE)-$(CI_COMMIT_SHORT_SHA)
	docker rmi $(CI_REGISTRY_IMAGE)/$(IMAGE)-$(CI_COMMIT_SHORT_SHA)
	docker logout

scan-ci:
	trivy --version
	# cache cleanup is needed when scanning images with the same tags, it does not remove the database
	time trivy image --clear-cache
	# update vulnerabilities db
	time trivy image --download-db-only
	# Builds report and puts it in the default workdir $CI_PROJECT_DIR, so `artifacts:` can take it from there
	# time trivy image --ignore-policy trivy-ignore.rego --exit-code 0 --format template --template "@/contrib/html.tpl" --output $(CI_PROJECT_DIR)/trivy-scanning-report.html $(IMAGE)
    # Prints full report
	trivy image --ignore-policy trivy-ignore.rego --light -s "UNKNOWN,MEDIUM,HIGH,CRITICAL" --exit-code 1 $(IMAGE)
