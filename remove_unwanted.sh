#!/bin/bash

for i in '0.13.7' '0.14.11' '0.15.5' '1.0.11' '1.1.9' '1.2.9' '1.3.7' '1.6.6' '1.7.5' '1.8.5' ;
  do rm -rf /usr/local/bin/tf/versions/$i ; rm -f /usr/local/bin/terraform$i ;
done