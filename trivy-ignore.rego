package trivy

import data.lib.trivy

ignore_pkgs_git_lfs := {"golang.org/x/text","golang.org/x/net","golang.org/x/sys"}
ignore_vulnerability_ids_git_lfs := {"CVE-2022-32149","CVE-2022-27664","CVE-2022-41717","CVE-2021-44716","CVE-2022-29526","GHSA-vvpx-j8f3-3w6h", "CVE-2023-45288"}
ignore_vulnerability_ids_alpine := {"CVE-2023-0464","CVE-2023-27535"}
ignore_pkgs_conftest := {"github.com/docker/cli","github.com/docker/distribution","github.com/open-policy-agent/opa","golang.org/x/net","github.com/aws/aws-sdk-go","github.com/containerd/containerd"}
ignore_vulnerability_ids_atlantis := {"CVE-2014-3499","CVE-2014-6407","CVE-2014-9356","CVE-2015-3627","CVE-2014-9358","CVE-2014-9357","CVE-2014-5277","CVE-2020-13401","CVE-2022-24769","CVE-2023-0475"}
ignore_vulnerability_ids_conftest := {"CVE-2021-41092","GMS-2022-20","CVE-2022-23628","CVE-2022-41723","CVE-2020-8911","CVE-2023-25173","CVE-2023-25153","GHSA-vvpx-j8f3-3w6h","CVE-2022-23471","CVE-2023-39325","CVE-2023-3978","CVE-2023-44487", "CVE-2024-8260"}
ignore_vulnerability_ids_golang := {"CVE-2018-1098","CVE-2018-1099","CVE-2023-28642","CVE-2022-41721","CVE-2022-28948"}
ignore_vulnerability_ids_golangcrypto := {"CVE-2022-27191","CVE-2023-48795"}
ignore_vulnerability_ids_containerd := {"CVE-2022-23648"}
ignore_vulnerability_ids_googlegolang := {"GHSA-m425-mq94-257g","CVE-2023-44487"}
ignore_pkgs_default := {"busybox", "ssl_client"}

default ignore = false

# Ignore unpatched libraries in alpine 3.15.0
# This block should be removed after a patched Atlantis Dockerfile is released
ignore {
    input.VulnerabilityID == ignore_vulnerability_ids_alpine[_]
}

# Ignore unpatched librarries in Atlantis 0.19.2
# This block should be remove after a patched Atlantis version is released
ignore {
    input.PkgName == "github.com/opencontainers/runc"
    input.VulnerabilityID == ignore_vulnerability_ids_atlantis[_]
}

# Ignore unpatched librariries in git-lfs
# This block should be remove after a patched git-lfs version is released
ignore {
    input.VulnerabilityID == ignore_vulnerability_ids_git_lfs[_]
}

# Ignore unpatched libraries in conftest 0.30.0
# This block should be removed after a patched conftest version is released
ignore {
    input.PkgName == ignore_pkgs_conftest[_]
    input.VulnerabilityID == ignore_vulnerability_ids_conftest[_]
}

ignore {
    input.PkgName == "github.com/docker/docker"
    input.VulnerabilityID == ignore_vulnerability_ids_containerd[_]
}

# Ignore unused runc code in gosu
ignore {
	# This finding can be safely ignored. See: https://github.com/tianon/gosu/issues/100
	input.PkgName == "github.com/opencontainers/runc"
	input.VulnerabilityID == "CVE-2021-43784" 
}
ignore {
	# This finding can be safely ignored. See: https://github.com/tianon/gosu/issues/100
	input.PkgName == "github.com/opencontainers/runc"
	input.VulnerabilityID == "CVE-2023-28642" 
}

ignore {
	# This finding can be safely ignored. See: https://github.com/tianon/gosu/issues/100
	input.PkgName == "github.com/opencontainers/runc"
	input.VulnerabilityID == "CVE-2023-27561" 
}

ignore {
    input.PkgName == "github.com/hashicorp/go-getter"
    input.VulnerabilityID == "CVE-2023-0475"
}

ignore {
    input.PkgName == "github.com/hashicorp/go-getter/v2"
    input.VulnerabilityID == "CVE-2023-0475"
}

ignore {
    input.PkgName == "github.com/opencontainers/runc"
    input.VulnerabilityID == "CVE-2022-29162"
}

ignore {
    input.PkgName == "github.com/docker/distribution"
    input.VulnerabilityID == "GHSA-qq97-vm5h-rrhg"
}

ignore {
    input.PkgName == "github.com/docker/docker"
    input.VulnerabilityID == "GHSA-jq35-85cj-fj4p"
}

ignore {
    input.PkgName == "github.com/cloudflare/circl"
    input.VulnerabilityID == "GHSA-9763-4f94-gfch"
}

ignore {
    input.PkgName == "google.golang.org/grpc"
    input.VulnerabilityID == ignore_vulnerability_ids_googlegolang[_]
}

ignore {
    input.PkgName == "go.etcd.io/etcd"
    input.VulnerabilityID == ignore_vulnerability_ids_golang[_]
}

ignore {
    input.PkgName == "golang.org/x/crypto"
    input.VulnerabilityID == ignore_vulnerability_ids_golangcrypto[_]
}

## Default packages ##
ignore {
    input.PkgName == "git"
    input.VulnerabilityID == "CVE-2022-24765"
}

ignore {
    input.PkgName == ignore_pkgs_default[_]
    input.VulnerabilityID == "CVE-2022-28391"
}

