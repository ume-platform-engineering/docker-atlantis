# Atlantis

## TL;DR

A docker image for running [atlantis](https://www.runatlantis.io/) with additional tooling added.

This image is used within [tf-atlantis](https://gitlab.com/ume-platform-engineering/tf-atlantis).

## Upgrading

To upgrade the version of atlantis just change the `ATLANTIS_VERSION` in the `Makefile`.

The version needs to align with an image tag from [here](https://hub.docker.com/r/runatlantis/atlantis/tags).


## Trivy findings and ignores
### Upgrade to 0.27.1
With the latest upgrade to atlantis image (from 0.24.3 to 0.27.1) there are a few new vulnerabilities and lease see reason for them to be added to the trivy ignore file(located here: ):

| Vulnerability ID| Reason                                           | Severity          |
| ----------------|--------------------------------------------------|-------------------|
| CVE-2023-48795 | https://nvd.nist.gov/vuln/detail/CVE-2023-48795  | Medium, stil analysed |
|CVE-2023-39325| https://nvd.nist.gov/vuln/detail/CVE-2023-39325  | High              |
|CVE-2023-3978| https://nvd.nist.gov/vuln/detail/CVE-2023-3978   | Medium, still analyed |
|CVE-2023-44487| https://nvd.nist.gov/vuln/detail/CVE-2023-44487  | High              |
|GHSA-jq35-85cj-fj4p| https://github.com/advisories/GHSA-jq35-85cj-fj4p| Moderate          |
| GHSA-m425-mq94-257g| https://github.com/advisories/GHSA-m425-mq94-257g |  High             |
|GHSA-9763-4f94-gfch| https://github.com/advisories/GHSA-9763-4f94-gfch | High              |
