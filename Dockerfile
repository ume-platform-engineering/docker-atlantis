ARG ATLANTIS_VERSION="Unknown"
FROM ghcr.io/runatlantis/atlantis:${ATLANTIS_VERSION}

LABEL MAINTAINER Platform Engineering <platform@underwriteme.co.uk>

# Install Python 3 and additional libraries
USER root
RUN apk update && apk add --no-cache \
  jq \
  python3 \
  aws-cli \
  && rm -rf /var/cache/apk/*

RUN apk add --update --no-cache python3 gcc libxslt-dev libxml2-dev libxml2 libxslt build-base python3-dev py3-pip py3-setuptools py3-wheel && \
  if [ ! -e /usr/bin/python ]; then ln -sf python3 /usr/bin/python ; fi && \
  if [ ! -e /usr/bin/pip ]; then ln -s pip3 /usr/bin/pip ; fi

# upgrade packages to fix vulnerabilities
RUN apk add --no-cache --upgrade curl git libcrypto3

ADD ./remove_unwanted.sh /home/atlantis
RUN /home/atlantis/remove_unwanted.sh
USER atlantis